#!/usr/bin/env bash
USAGE="rename_fastqs.sh -b <BASE DATA DIR>"
while getopts ":b:" option
do
    case "${option}" in
        b) BASE_DIR=${OPTARG};;
        \?) echo "Invalid option: -${OPTARG}" >&2
            exit 1;;
        :) echo "Option -${OPTARG} requires an argument" >&2
            exit 1;;
    esac
done

# Test for data dir
if [[ -z $BASE_DIR ]]
then
    echo "You must specify a base directory using -b"
    echo "${USAGE}"
    exit
fi

if [[ ! -d ${BASE_DIR}/all_fastqs_to_date ]]
then
    echo "The base directory ${BASE_DIR} you specified using -b does not exist or it does not contain the sub-directory all_fastqs_to_date"
    echo "${USAGE}"
    exit
fi

# make renamed_fastqs if required
if [[ ! -d ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs ]]; then
    mkdir ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs
fi

# Check for GHRU ID == NA
# This value should be 0
NUM_SAMPLES_WITH_NA=$(awk '$3 == "NA" {print $0}'  ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.txt | wc -l)
SAMPLES_WITH_NA=$(awk '$3 == "NA" {print $1 "=>" $3}'  ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.txt)
for SAMPLE in ${SAMPLES_WITH_NA}
do
    echo "ERROR, GHRU ID = NA: ${SAMPLE}"
done
if [[ ${NUM_SAMPLES_WITH_NA} -gt 0 ]]
then
    echo "ERROR: THERE ARE ${NUM_SAMPLES_WITH_NA} SAMPLES WHERE GHRU ID == 'NA'"
    exit
fi

# Check for duplicates
NUM_DUPLICATE_SAMPLES=$(sort -k3 ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.txt | uniq -f2 -d | wc -l)
NUM_DUPLICATES=$(sort -k3 ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.txt | uniq -f2 -D | wc -l)
DUPLICATE_SAMPLES=$(sort -k3 ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.txt | uniq -f2 -D | awk '{print $1 "=>" $3}')
for SAMPLE in ${DUPLICATE_SAMPLES}
do
    echo "ERROR, DUPLICATE SAMPLE: ${SAMPLE}"
done
if [[ ${NUM_DUPLICATE_SAMPLES} -gt 0 ]]
then
    echo "ERROR: THERE ARE ${NUM_DUPLICATE_SAMPLES} DUPLICATE SAMPLES (${NUM_DUPLICATES} duplicates)"
    exit
fi

NUM_RENAMED_FASTQS=0
echo "Renaming fastqs ....... (Please wait)"
while read LINE
do
    SANGER_LANE_ID=$(echo ${LINE} | awk {'print $1'})
    GHRU_ID=$(echo ${LINE} | awk {'print $3'})
    # check not already processed
    if ! grep ${GHRU_ID} -q ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt
        then
        # link read 1
        if [[ -f ${BASE_DIR}/all_fastqs_to_date/fastqs/${SANGER_LANE_ID}_1.fastq.gz && ! -f ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_1.fastq.gz ]]
        then
            ln -s $(readlink -f ${BASE_DIR}/all_fastqs_to_date/fastqs/${SANGER_LANE_ID}_1.fastq.gz) ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_1.fastq.gz
            # add file to all_fastqs.txt
            if ! grep -q ${GHRU_ID}_1.fastq.gz ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt
            then
                echo ${GHRU_ID}_1.fastq.gz >> ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt
            fi
            
            # increment counter
            NUM_RENAMED_FASTQS=$((NUM_RENAMED_FASTQS +1))

        elif [[ -f ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_1.fastq.gz ]]
        then
            echo "ERROR: File ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_1.fastq.gz already exists. Possible duplicate"
            exit
        fi

        # link read 2
        if [[ -f ${BASE_DIR}/all_fastqs_to_date/fastqs/${SANGER_LANE_ID}_2.fastq.gz && ! -f ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_2.fastq.gz ]]
        then
            ln -s $(readlink -f ${BASE_DIR}/all_fastqs_to_date/fastqs/${SANGER_LANE_ID}_2.fastq.gz) ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_2.fastq.gz
            # add file to all_fastqs.txt
            if ! grep -q ${GHRU_ID}_2.fastq.gz ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt
            then
                echo ${GHRU_ID}_2.fastq.gz >> ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt
            fi
        elif [[ -f ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_2.fastq.gz ]]
        then
            echo "ERROR: File ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${GHRU_ID}_2.fastq.gz already exists. Possible duplicate"
            exit
        fi
    fi
done < ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.txt

NUM_SAMPLES_IN_FASTQS=$(ls ${BASE_DIR}/all_fastqs_to_date/fastqs/*_1.fastq.gz | wc -l)
NUM_SAMPLES_IN_RENAMED_FASTQS=$(ls ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/*_1.fastq.gz | wc -l)
if [[ $NUM_SAMPLES_IN_FASTQS == $NUM_SAMPLES_IN_RENAMED_FASTQS ]]
then
    echo "The project in ${BASE_DIR} now contains ${NUM_SAMPLES_IN_FASTQS} samples"
else
    echo "Number of fastqs with Sanger Lane IDs (${NUM_SAMPLES_IN_FASTQS}) does not match number of samples in renamed fastqs (${NUM_SAMPLES_IN_RENAMED_FASTQS}). Please investigate!!"
fi

echo "Renamed ${NUM_RENAMED_FASTQS} fastqs in the latest batch"