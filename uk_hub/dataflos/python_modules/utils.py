import logging
import colorlog
import os
import sys
import argparse

def get_logger(filepath, level=logging.DEBUG):
    # create logger
    logger = logging.getLogger(os.path.basename(filepath))
    logger.setLevel(level)
    # create console handler and set level to debug
    console = logging.StreamHandler()
    console.setLevel(level)
    # create formatter
    format_str = '%(asctime)s - %(levelname)-8s - %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    if os.isatty(2):
        cformat = '%(log_color)s' + format_str
        colors = {'DEBUG': 'reset',
                  'INFO': 'reset',
                  'WARNING': 'bold_yellow',
                  'ERROR': 'bold_red',
                  'CRITICAL': 'bold_red'}
        formatter = colorlog.ColoredFormatter(cformat, date_format,
                                              log_colors=colors)
    else:
        formatter = logging.Formatter(format_str, date_format)
    # add formatter to ch
    console.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(console)

    return logger

class ParserWithErrors(argparse.ArgumentParser):
    logger = get_logger(__file__)
    def error(self, message):
        self.logger.error('{0}\n\n'.format(message))
        self.print_help()
        sys.exit(1)

    def is_valid_file(self, parser, arg):
        if not os.path.isfile(arg):
            parser.error("The file %s does not exist!" % arg)
        else:
            return arg