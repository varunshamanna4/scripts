#!/usr/bin/env python3
import requests
import argparse
import os
import re
from time import sleep

from python_modules.spinner import Spinner
from python_modules.utils import ParserWithErrors
from python_modules.utils import get_logger

def parse_arguments():
    description = """
    A script to update the Google MLST data sheets for each country
    """
    # parse all arguments
    parser = ParserWithErrors(description = description)
    parser.add_argument("-m", "--mlst_file",
                        help="File path to combined_mlst_report.tsv",
                        type=lambda x: parser.is_valid_file(parser, x),
                        required = True
                        )
    parser.add_argument('-s', '--species', help='Species name (Genus species)', required = True)
    parser.add_argument('-v', '--verbose', help='Print out created/updated ids', action='store_true')
    country = parser.add_mutually_exclusive_group(required = True)
    country.add_argument('-c', '--country', help='Country name(s). comma separated list')
    country.add_argument('-a', '--all_countries', action='store_true', help='Run update for all countries')

    options = parser.parse_args()
    return options
  

def update_mlst(mlst_file, species, country):
      print()
      multipart_form_data = {
          'MLST combined report': (os.path.basename(mlst_file), open(mlst_file, 'rb')),
          'species': (None, species),
          'country': (None, country)
      }

      dataflo_url = 'https://data-flo.io/api/dataflows/run/hAp8rBg9M1k8beaPHyg1Xf'
      for attempt in range(1,6):
        with Spinner(f'Updating Google Sheet for {country} (attempt {attempt}) '):
        # print(f'Updating Google Sheet for {country} (attempt {attempt})')
          response = requests.post(dataflo_url,files=multipart_form_data)
        if response.status_code == 200:
          break

      print()
      print(f'\tResponse code: {response.status_code}')
      if response.status_code != 200:
        print(f'\tError: {response.content.decode("utf-8")}')
        return None
      else:
        return response.json()

def get_created_and_updated_ids(responses):
  created_and_updated_ids = set()
  for country in responses.keys():
      created_and_updated_ids.update(responses[country]['created'])
      created_and_updated_ids.update(responses[country]['updated'])
  return created_and_updated_ids

def get_invalid_ids(responses):
    invalid_ids = set()
    for country in responses.keys():
      invalid_ids.update(responses[country]['invalid GHRU ids'])
    invalid_ids = invalid_ids - get_created_and_updated_ids(responses)
    return invalid_ids

def valid_id_for_country(id, country):
  # first test if valid for GHRU format
  country_ranges = {
    'Colombia' : (1,250000),
    'India' : (250001,500000),
    'Nigeria' : (500001,750000),
    'Philippines' : (750001,999999)
  }
  if re.match(r'G\d{8}', id):
    six_digits = int(re.match(r'G\d{2}(\d{6})', id).groups()[0])
    if six_digits >= country_ranges[country][0] and six_digits <= country_ranges[country][1]:
      return True
  return False

def print_response(response, country, verbose = False):
      if verbose:
        print(f'\tIDs created:\n\t\t{sorted(response["created"])}')
        print(f'\tIDs updated:\n\t\t{sorted(response["updated"])}')
      else:
        print(f'\tIDs created: {len(response["created"])}')
        print(f'\tIDs updated: {len(response["updated"])}')
      
      invalid_ids = [id for id in response['invalid GHRU ids'] if valid_id_for_country(id, country)]
      print(f'\tInvalid IDs :\n\t\t{sorted(invalid_ids)}')
      print()

if __name__ == "__main__":
    options = parse_arguments()
    responses = {}
    if options.country:
      countries = options.country.split(',')
    elif options.all_countries:
      countries = ['Colombia', 'India', 'Nigeria', 'Philippines']
    
    countries_with_failed_updates = []
    for country in countries:
      response = update_mlst(options.mlst_file, options.species, country)
      if response:
        print_response(response, country, options.verbose)
      else:
        countries_with_failed_updates.append(country)
      print('Waiting for 1 minute so as to prevent API limit breaches')
      sleep(60)
    
    if len(countries_with_failed_updates) > 0:
      print(f'The following countries failed to update. Retry using -c {",".join(countries_with_failed_updates)} instead of -a')
    # show invalid IDs
    # invalid_ids = get_invalid_ids(responses)
    # print(f'Invalid ids:\n\t{sorted(invalid_ids)}')
