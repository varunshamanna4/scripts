#!/usr/bin/env bash
USAGE="get_latest_fastqs_for_processing.sh -b <BASE DATA DIR>"
while getopts ":b:" option
do
    case "${option}" in
        b) BASE_DIR=${OPTARG};;
        \?) echo "Invalid option: -${OPTARG}" >&2
            exit 1;;
        :) echo "Option -${OPTARG} requires an argument" >&2
            exit 1;;
    esac
done

# Test for data dir
if [[ -z $BASE_DIR ]]
then
    echo "You must specify a base directory using -b"
    echo "${USAGE}"
    exit
fi

if [[ ! -d ${BASE_DIR}/all_fastqs_to_date ]]
then
    echo "The base directory ${BASE_DIR} you specified using -b does not exist or it does not contain the sub-directory all_fastqs_to_date"
    echo "${USAGE}"
    exit
fi

# check that there are fastqs to process
if [[ -f ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt ]]
then
    LATEST_FASTQS=$(diff -u  <(sort ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt) <(sort ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt) | grep -E "^-" | sed -E 's/^-//' | tail -n+2 )
else
    LATEST_FASTQS=$(cat ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt)
fi

NUM_LATEST_FASTQS=$(wc -l < ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt)

if [[ ${NUM_LATEST_FASTQS} -eq 0 ]]
then
    echo "There are no fastqs to process"
    exit
fi

DATE=$(date '+%Y-%m-%d')
echo "Making batch directory for ${DATE} and sym linking fastqs"

if [[ -f ${BASE_DIR}/all_fastqs_to_date/batches/${DATE}.txt || -d ${BASE_DIR}/batches/${DATE} ]]; then
    echo "You have already processed a batch of samples today (date=${DATE})"
    exit
fi

# make batch directories if required
if [[ ! -d ${BASE_DIR}/all_fastqs_to_date/batches  ]]; then
    mkdir ${BASE_DIR}/all_fastqs_to_date/batches 
fi

if [[ ! -d ${BASE_DIR}/batches  ]]; then
    mkdir ${BASE_DIR}/batches 
fi

# make directory for batch and sub-dir for fastqs
mkdir -p ${BASE_DIR}/batches/${DATE}/fastqs

for FASTQ in ${LATEST_FASTQS}
do
    # link read
    if [[ -f ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${FASTQ} && ! -f ${BASE_DIR}/batches/${DATE}/fastqs/${FASTQ} ]]
    then
        ln -s $(readlink -f ${BASE_DIR}/all_fastqs_to_date/renamed_fastqs/${FASTQ}) ${BASE_DIR}/batches/${DATE}/fastqs/${FASTQ}
    fi
    # add read to batch file
    echo ${FASTQ} >> ${BASE_DIR}/all_fastqs_to_date/batches/${DATE}.txt
done

# make new analysis directory including a scripts and cluster_logs dir
mkdir -p ${BASE_DIR}/batches/${DATE}
mkdir ${BASE_DIR}/batches/${DATE}/scripts
mkdir ${BASE_DIR}/batches/${DATE}/cluster_logs

# update fastqs that have been processed
cp ${BASE_DIR}/all_fastqs_to_date/all_fastqs.txt ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt

NUM_LATEST_SAMPLES=$(ls ${BASE_DIR}/batches/${DATE}/fastqs/*_1.fastq.gz | wc -l)
echo "There are ${NUM_LATEST_SAMPLES} samples to process in ${BASE_DIR}/batches/${DATE}"
echo "Please commit latest changes:"
echo "cd ${BASE_DIR}/all_fastqs_to_date; git add .; git commit -m 'new batch ${DATE}'; git push origin master"
