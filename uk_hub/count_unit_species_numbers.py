#!/usr/bin/env python3
import os
from glob import glob

base_dir = "/lustre/scratch118/infgen/team212/cgps/ghru/retrospective_1"
units = ["colombia_5606", "india_5605", "nigeria_5604", "philippines_5603"]
species_counts = {'pass_and_warning' : {}, 'failure' : {}}


def count_species(base_dir, units, species_counts, qc_type):
    for unit in units:
        for species_dir in sorted(glob(f'{base_dir}/{unit}/project_species_fastqs/*')):
            if "no_significant_matches" not in species_dir:
                species = os.path.basename(species_dir)
                # initialise for species
                if species not in species_counts[qc_type]:
                    species_counts[qc_type][species] = {}
                    for init_unit in units:
                        species_counts[qc_type][species][init_unit] = 0
                if qc_type == 'pass_and_warning':
                    num_samples = len(glob(f'{species_dir}/*_1.fastq.gz'))
                elif qc_type == 'failure':
                    num_samples = len(glob(f'{species_dir}/FAILURE/*_1.fastq.gz'))
                species_counts[qc_type][species][unit] = num_samples

def get_species_count_tsv_string(units, species_counts, qc_type):
    unit_totals = {unit: 0 for unit in units}
    tsv_string = f'\t{chr(9).join(units)}\ttotal\n'
    for species in sorted(species_counts[qc_type].keys()):
        counts = [species_counts[qc_type][species][unit] for unit in units]
        total = sum(counts)
        tsv_string += f'{species}\t{chr(9).join([str(count) for count in counts])}\t{total}\n'
        for unit in units:
            # only include Salmonella enterica not the serovars
            if 'salmonella_enterica_subsp._enterica' not in species:
                unit_totals[unit] += species_counts[qc_type][species][unit]
    grand_total = sum([unit_totals[unit] for unit in units])
    tsv_string += f'total\t{chr(9).join(str(unit_totals[unit]) for unit in units)}\t{grand_total}\n'
    return tsv_string

count_species(base_dir, units, species_counts, 'pass_and_warning')
count_species(base_dir, units, species_counts, 'failure')


print("PASS and WARNING samples")
print("========================")
tsv_file_name = f'{base_dir}/pass_and_warning_sample_count.tsv'
with open(tsv_file_name, 'w') as tsv_file:
    tsv_file.write(get_species_count_tsv_string(units, species_counts, 'pass_and_warning'))
print(f'Written to {tsv_file_name}\n')

print("FAILURE samples")
print("===============")
tsv_file_name = f'{base_dir}/failure_sample_count.tsv'
with open(tsv_file_name, 'w') as tsv_file:
    tsv_file.write(get_species_count_tsv_string(units, species_counts, 'failure'))
print(f'Written to {tsv_file_name}\n')

