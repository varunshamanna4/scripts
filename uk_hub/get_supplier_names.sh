#!/usr/bin/env bash
USAGE="get_supplier_names.sh -b <BASE DATA DIR> -s <SAMPLE INFO FILE SUFFIX> -d <DATABASE> -o <OUTPUT_FILE> -q"
while getopts ":b:s:d:o:q" option
do
    case "${option}" in
        b) BASE_DIR=${OPTARG};;
        s) FILE_SUFFIX=${OPTARG};;
        d) DATABASE=${OPTARG};;
        o) OUTPUT_FILE=${OPTARG};;
        q) QUIET="true";;
        \?) echo "Invalid option: -${OPTARG}" >&2
            exit 1;;
        :) echo "Option -${OPTARG} requires an argument" >&2
            exit 1;;
    esac
done

# Test for data dir
if [[ -z $BASE_DIR ]]
then
    echo "You must specify a base directory using -b"
    echo "${USAGE}"
    exit
fi

# Test for file suffix
if [[ -z $FILE_SUFFIX ]]
then
    echo "You must specify a suffix for the sample info file using -s"
    echo "${USAGE}"
    exit
fi

if [[ ! -f ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.${FILE_SUFFIX} ]]
then
    echo "The base directory ${BASE_DIR} you specified using -b does not exist or it does not contain the all_fastqs_to_date/samples.lane_info.txt file"
    echo "${USAGE}"
    exit
fi

SAMPLE_INFO_FILE=${BASE_DIR}/all_fastqs_to_date/samples.lane_info.${FILE_SUFFIX}

if [[ -z $DATABASE ]]
then
    DATABASE="pathogen_cgps_track"
fi

if [[ -z $OUTPUT_FILE ]]
then
    OUTPUT_FILE="supplier_names.txt"
fi

if [[ -z $QUIET ]]
then
    QUIET="false"
fi

OUTPUT_FILE=${BASE_DIR}/all_fastqs_to_date/${OUTPUT_FILE}

#DATABASE QUERIES
LANES=$(tail -n +2 $SAMPLE_INFO_FILE | awk {'print $1'} | sed "s/^\|$/\'/g"|paste -sd, -)

echo "select l.name, s.name from latest_lane l JOIN latest_library lib ON lib.library_ID=l.library_ID  JOIN latest_sample s ON s.sample_ID = lib.sample_ID where l.name in ($LANES);">command.sql
mysql -h patp-db -P 3347 -u pathpipe_ro -N -D $DATABASE -t < command.sql > lanes_and_samples.txt
rm command.sql 

SAMPLES=$(cat lanes_and_samples.txt | awk '{for(i=4;i<=NF;++i)print $i}' | grep -v "|" | sed '/^[[:space:]]*$/d' | sed "s/^\|$/\'/g" | sed -n -e 'H;${x;s/\n/,/g;s/^,//;p;}')
echo "select name, supplier_name from sample where name in ($SAMPLES)" > command.sql
MYSQL_PWD=pathogen8734 mysql -h mlwh-db -P 3435 -u mlwh_pathogen -N -D mlwarehouse -t < command.sql > samples_and_supplier_names.txt

#PUT TABLES TOGETHER 
# get rid of header, footer and sql separators
cat lanes_and_samples.txt | tail -n +2 | head -n -1 | sed 's/^| *//' | sed 's/ *|$//' | sed 's/ *| */\t/' | sort -k 2 > lanes_and_samples.tsv
cat samples_and_supplier_names.txt| tail -n +2 | head -n -1 | sed 's/^| *//' | sed 's/ *|$//' | sed 's/ *| */\t/' | sort -k 1 > samples_and_supplier_names.tsv

echo "lane	sample	supplier_name" > $OUTPUT_FILE
join -1 2 -2 1 lanes_and_samples.tsv samples_and_supplier_names.tsv | awk 'BEGIN {OFS="\t"; IFS="\t"}{print $2,$1,$3}' >> $OUTPUT_FILE

#  clean up
rm lanes_and_samples.txt samples_and_supplier_names.txt lanes_and_samples.tsv samples_and_supplier_names.tsv command.sql

#CHECK OUTPUT 
if [ "$(wc -l < $SAMPLE_INFO_FILE)" -eq "$(wc -l < $OUTPUT_FILE)" ]``
then
    if [[ $QUIET == "false" ]]
    then
        echo "success: output in $OUTPUT_FILE";
    fi
else 
    echo 'missing lanes'
    tail -n +2 $OUTPUT_FILE | awk '{print $2}' > tmp_output_lanes
    tail -n +2 $SAMPLE_INFO_FILE | awk '{print $1}' > tmp_input_lanes
    diff <(sort tmp_input_lanes) <(sort tmp_output_lanes)
    rm tmp_input_lanes tmp_output_lanes
fi

