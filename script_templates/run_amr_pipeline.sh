NEXTFLOW_PIPELINE_DIR=/data/nextflow_pipelines
DATA_DIR=/data/projects/ghru_retrospective_2018
SPECIES=<SPECIES>
ARIBA_POINTFINDER_SPECIES=""
for TEST in campylobacter enterococcus_faecalis enterococcus_faecium escherichia_coli helicobacter_pylori klebsiella mycobacterium_tuberculosis neisseria_gonorrhoeae salmonella staphylococcus_aureus
do
    if [[ $SPECIES == $TEST* ]]
    then
        ARIBA_POINTFINDER_SPECIES=$TEST
    fi
done

if [[ $ARIBA_POINTFINDER_SPECIES != "" ]]
then
    NXF_VER=20.01.0-rc1 nextflow run ${NEXTFLOW_PIPELINE_DIR}/amr_prediction/main.nf\
    --input_dir ${DATA_DIR}/project_species_fastqs/${SPECIES} \
    --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
    --output_dir ${DATA_DIR}/amr_reports/${SPECIES} \
    --read_polishing_adapter_file ${NEXFLOW_WORKFLOWS_DIR}/amr-prediction/adapters.fas \
    --read_polishing_depth_cutoff 100 \
    --species ${ARIBA_POINTFINDER_SPECIES} \
    -w ${DATA_DIR}/amr_reports/${SPECIES}/work \
    -resume
else
    NXF_VER=20.01.0-rc1 nextflow run ${NEXTFLOW_PIPELINE_DIR}/amr_prediction/main.nf\
    --input_dir ${DATA_DIR}/project_species_fastqs/${SPECIES} \
    --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
    --output_dir ${DATA_DIR}/amr_reports/${SPECIES} \
    --read_polishing_adapter_file ${NEXFLOW_WORKFLOWS_DIR}/amr-prediction/adapters.fas \
    --read_polishing_depth_cutoff 100 \
    -w ${DATA_DIR}/amr_reports/${SPECIES}/work \
    -resume
fi
