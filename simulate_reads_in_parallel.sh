# assume a directory structure with genomes in fastas/*.fna and a directory for fastqs
simulate_reads() {
  BASE_DIR=$PWD
  FILE=$1
  PREFIX=${FILE%_genomic.fna}
  PREFIX=${PREFIX#fastas/*}
  pirs simulate -l 100 -x 50 -m 500 \
  --no-substitution-errors --no-indel-errors --no-gc-content-bias \
  --threads=1 --random-seed=12345 \
  -o ${BASE_DIR}/fastqs/${PREFIX} -n \
  ${BASE_DIR}/${FILE} > /dev/null 2>&1
  mv ${BASE_DIR}/fastqs/${PREFIX}_100_500_1.fq ${BASE_DIR}/fastqs/${PREFIX}_1.fastq
  mv ${BASE_DIR}/fastqs/${PREFIX}_100_500_2.fq ${BASE_DIR}/fastqs/${PREFIX}_2.fastq
  gzip ${BASE_DIR}/fastqs/${PREFIX}_1.fastq
  gzip ${BASE_DIR}/fastqs/${PREFIX}_2.fastq
}
export -f simulate_reads
parallel --eta simulate_reads ::: fastas/*.fna
