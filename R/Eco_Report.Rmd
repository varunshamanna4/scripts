---
title: "Eco Analysis Retrospective 1"
author: "Anthony Underwood"
date: "26/06/2020"
output:
  html_document: 
    df_print: kable
    toc: yes
  pdf_document:
    df_print: kable
  word_document: default
---
```{r warning=FALSE, message=FALSE, echo=FALSE}
source("functions/multi_country_functions.R")
# Read in data
all_unit_eco_metadata <- readr::read_tsv("/Volumes/GoogleDrive/My Drive/sequence_data/ghru_retrospective/all/microreacts/escherichia_coli/fixed_complete_metadata.tsv")
# remove ref
sample_metadata <- all_unit_eco_metadata %>% dplyr::rename(ST = ST__autocolour) %>%  tidyr::drop_na(Country)

# Get data about NCBI AMR gene
ncbi_amr_metadata <- get_ncbi_amr_metadata()

# Get point mutattion metadata
point_mutation_metadata <- get_point_mutation_metadata("escherichia_coli")
```
### Summary of numbers from each country
Table 1: Describing total number of E.coli samples submitted by each country for Retrospective 1
```{r echo=FALSE, message=FALSE, warning=FALSE}
# Count numbers
samples_per_country <-  count_samples_by_country(sample_metadata, add_total = TRUE)

# print table
samples_per_country
```

### Frequency of STs by country
Looking at the 5 most frequent STs in each country, different distribution patterns are observed.

 - In Colombia ST131 and ST648 are the most frequent STs
 - In India STs 10, 131, 2914 and 394 are the most frequent
 - In Nigeria, whilst awaiting further metadata, ST131 is currently the most frequent ST
 - In The Philippines ST 1193, 131, and 38 are the most common STs.
 
 Although there are differences the frequency of ST131 demonstrates the veracity of [this paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5333602/) and it's headline "Escherichia coli ST131: a multidrug-resistant clone primed for global domination"
```{r echo=FALSE, fig.height=6, fig.width=10, message=FALSE, warning=FALSE}
# calculate counts per country
samples_per_country <-  count_samples_by_country(sample_metadata)

# calculate st counts per country
st_counts_by_country <- count_sts_by_country(sample_metadata)

# calculate STs that are in the top 5 for each country
most_common_sts_data <- count_most_common_sts_per_country(st_counts_by_country)
# Plot STs
common_sts_plot <- plot_most_common_sts(most_common_sts_data)
print(common_sts_plot)
```

### Analyse key bug drug combinations based on NCBI predictions
When focussing on the key bug/drug combinations (ESBLs and Carbapenamases), different frequencies of carbapenamse _Escherichia coli_ can be observed and in general the frequency is lower than when compared with carbapenamse-producing _Klebsiella pneumoniae_.
The genes responsible for carbapenamse resistance differ between countries. In Colombia, KPC is the dominant carbapenemase, whilst in India and The Philippines, NDM is the key AMR determinant. In India there are also blaOXA genes responsible for carbapenem resistance.
```{r echo=FALSE, fig.height=10, fig.width=10, message=FALSE, warning=FALSE}
# get long format data
acquired_and_point_amr_data <- pivot_metadata_to_long_amr(sample_metadata, ncbi_amr_metadata, point_mutation_metadata)
long_acquired_gene_amr_data <- acquired_and_point_amr_data[[1]]
acquired_gene_colnames <-  acquired_and_point_amr_data[[2]]
long_point_mutation_amr_data <- acquired_and_point_amr_data[[3]]
point_mutation_colnames <-  acquired_and_point_amr_data[[4]]
# key drug classes
key_drug_subclasses <- c("BETA-LACTAM", "CEPHALOSPORIN", "CARBAPENEM")

subclass_counts_by_country <- count_AMR_subclasses_by_country(long_acquired_gene_amr_data, samples_per_country, key_drug_subclasses)
AMR_subclasses_plot <- plot_AMR_subclasses_by_country(subclass_counts_by_country)

gene_family_counts_by_country <- count_gene_families_by_country(long_acquired_gene_amr_data, subclass_counts_by_country, key_drug_subclasses)
gene_family_dot_plot <- dot_plot_gene_family_counts_by_country(gene_family_counts_by_country)
# gridExtra::grid.arrange(AMR_subclasses_plot, gene_family_dot_plot, ncol = 1, heights = 2:3)
plot_grid(AMR_subclasses_plot, gene_family_dot_plot,ncol =1, align="v", rel_heights = c(1, 3))

```


### Gene alleles responsible for resistances to Cephalosporins and Carbapenems
It is important to look at the gene alleles responsibel for resistance. For example CTXM-15 is responsible for cephalosporin resistance in all countries as is blaOXA-1. In The Philippines blaOXA-10 is also observed.
```{r echo=FALSE, fig.height=15, fig.width=10, message=FALSE, warning=FALSE}
drug_subclasses <- c('CEPHALOSPORIN', 'CARBAPENEM')
allele_counts_by_country <- count_alleles_by_country(long_acquired_gene_amr_data, gene_family_counts_by_country, drug_subclasses)
allele_counts_plots <- plot_allele_counts_by_country(allele_counts_by_country, drug_subclasses)
print(allele_counts_plots[[1]])
print(allele_counts_plots[[2]])
```

### Consolidate ariba clusters into subclasses
Using these classifications  the data can be grouped to make a cleaner Microreact
```{r message=FALSE, warning=FALSE}
consolidated_eco_metadata <- consolidate_ariba_cluster_names(all_unit_eco_metadata, long_acquired_gene_amr_data, long_point_mutation_amr_data)
```

```{r echo=FALSE, message=FALSE, warning=FALSE}
knitr::include_graphics("images/consolidated_eco_microreact.png")
```
[Microreact Link](https://microreact.org/project/qGULVYoKXCi8ng73hghDQy)




